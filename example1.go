// example1.go
package main

import (
	"fmt"

	parser "./parser"
	"github.com/antlr/antlr4/runtime/Go/antlr"
)

type binfunc func(int, int) int
type unfunc func(int) int

func identity(i int) int { return i }
func square(i int) int   { return i * i }

type MaprListener struct {
	*parser.BaseMaprListener

	base float64
	fmap unfunc
	fred binfunc
}

func (l *MaprListener) ExitMapper(c *parser.MapperContext) {
	s := c.Unfunc().GetText()
	if s == "identity" {
		l.fmap = identity
	}
	if s == "square" {
		l.fmap = square
	}
	fmt.Print("f(4) = ")
	fmt.Println(l.fmap(4))

}

func main() {
	is := antlr.NewInputStream("mapper = square")

	lexer := parser.NewMaprLexer(is)
	stream := antlr.NewCommonTokenStream(lexer, antlr.TokenDefaultChannel)

	p := parser.NewMaprParser(stream)

	var listener MaprListener
	antlr.ParseTreeWalkerDefault.Walk(&listener, p.Start())
}
