// Mapr.g4
grammar Mapr;

// Tokens

WHITESPACE: [ \r\n\t]+ -> skip;
EQUALS: '=';
MAPPER: 'mapper';
ID: 'identity';
SQUARE: 'square';

// Rules
start : mapper EOF;

mapper : MAPPER EQUALS unfunc # MapperStmt
;

unfunc
  : ID # IdentityStmt
  | SQUARE # SquareStmt
  ;
